#!/bin/bash
set -euo pipefail
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "[+] Installing ZSH..."
sudo apt-get update -y
sudo apt-get install zsh terminator git -y
chsh -s $(which zsh)

# echo "[+] Installing vscode..."
# wget "https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64" -O vscode.dpkg
# sudo dpkg -i vscode.dpkg
# rm -rf vscode.dpkg

echo "[+] Installing oh-my-zsh..."
wget "https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh" -O "install.sh"
chmod a+x "install.sh"
CHSH="no" RUNZSH="no" ./"install.sh"

echo "[+] Installing fonts..."
mkdir -p ~/.fonts
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Meslo.zip -O temp.zip
unzip temp.zip -d ~/.fonts/
rm temp.zip
fc-cache -f -v

echo "[+] Installing zsh-syntax-highlighting plugin..."
wget https://github.com/zsh-users/zsh-syntax-highlighting/archive/master.zip -O temp.zip
unzip temp.zip
mv zsh-syntax-highlighting-master /home/$(whoami)/.oh-my-zsh/plugins/zsh-syntax-highlighting
rm temp.zip

echo "[+] Installing powerlevel10k..."
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.oh-my-zsh/themes/powerlevel10k

echo "[+] Updating ~/zshrc..."
sed -i -e 's|.*ZSH_THEME.*|ZSH_THEME="powerlevel10k/powerlevel10k"|' ~/.zshrc
echo 'POWERLEVEL10K_MODE="nerdfont-complete"' >> ~/.zshrc
sed -i -e 's|.*plugins.*|plugins=(git dnf zsh-syntax-highlighting)|' ~/.zshrc
echo 'source $ZSH/oh-my-zsh.sh' >> ~/.zshrc

echo "[+] Updating terminator config..."
mkdir -p ~/.config/terminator
cp terminator_config ~/.config/terminator/config

echo "[+] Updating VSCode Config..."
cp vscode.json ~/.config/Code/User/settings.json
sudo chsh -s /bin/zsh $(whoami)
# 'builtin' 'source' "/home/$(whoami)/.oh-my-zsh/powerlevel10k/powerlevel10k.zsh-theme"


echo "[+] Install VScode extensions"
code --install-extension "ms-vscode.notepadplusplus-keybindings"
code --install-extension "yzhang.markdown-all-in-one"
code --install-extension "eamodio.gitlens"
code --install-extension "coenraads.bracket-pair-colorizer-2"
code --install-extension "ms-azuretools.vscode-docker"
code --install-extension "mikestead.dotenv"
code --install-extension "mhutchie.git-graph"
code --install-extension "golang.go"
code --install-extension "766b.go-outliner"
code --install-extension "hashicorp.terraform"
code --install-extension "ms-vsliveshare.vsliveshare"
code --install-extension "mohsen1.prettify-json"
code --install-extension "gruntfuggly.todo-tree"

read -p "reboot?[y/n] " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    reboot
fi

# After reboot, open terminator and run the following if you are not prompted to setup p10k
# p10k configure
